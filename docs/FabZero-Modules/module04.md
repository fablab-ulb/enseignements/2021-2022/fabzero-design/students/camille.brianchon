# Module 4_Découpe assistée par ordinateur

_Dans ce quatrième et dernier module, nous avons suivi une formation pour pouvoir utiliser seul et en toute sécurité les imprimantes **Laser**. Les étapes seront décrites en détails. Il nous était également demandé de **réaliser une lampe** à l'aide d'une **feuille de Polypropylène translucide** uniquement (pas de colle, vis, etc...). Mon travail et son évolution seront aussi détaillé dans ce module._

![](../images/fablab.jpg)


## A propos des imprimantes Laser

>**Précautions**
- Connaitre avec certitude le matériau utilisé
- Il faut toujours brancher la pompe à air
- Savoir où se trouvent les boutons pause et stop
- Il faut toujours allumer l’extracteur de fumée

> **Interface**
- Uniquement les fichiers vectoriels (SVG, DXF par exemple). Pour ma part j'ai utilisé des fichiers SVG en convertissant mes fichiers AutoCad DWG sur un [site](https://anyconv.com/fr/convertisseur-de-dwg-en-svg/) de conversion.

> **Matériaux**
- Recommandés : _bois contreplaqué, l’acrylique, le papier, le carton, les textiles, le plexi_
- Interdits : _PVC, le cuivre, le téflon (PTFE), le vinyl, le simili-cuir, la résine phénolique et l’époxy_

Voici des liens Git d'explications bien détaillées des 2 machines Laser du Fablab.

[Guide LASERSAUR](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/Lasersaur.md?fbclid=IwAR354xDSBdgEPC1EaZM0nkQpejiy3op5PMXGsxbDBE7CocadDIj0-iWcIYo)

[Guide EPILOG](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/EpilogFusion.md?fbclid=IwAR0fnaxFCTqt-emxQBV2HEzJzevotHc8wjv1Ha3iyew3-YQioIex2ovBHQs)


## Utilisation

1. En premier, il faut **remettre à zéro** (retour à l'origine), vérifier les statuts (indicateurs doivent être vert) et puis **ouvrir le fichier SVG ou DXF**.

2.  Ensuite, on peut **installer le matériau** dans la machine, puis **déplacer le laser** au bon endroit et **régler la distance focale** (entre lentille et matériau).

3. Pour finir, on peut **déplacer le dessin si besoin**, et finir par **régler la puissance et la vitesse de travail** ainsi que le choix des couleurs en fonction de si c'est de la découpe ou de la gravure.

**A NE PAS OUBLIER !!**
> - allumer le refroidissement à eau (bouton noir)
- allumer extracteur de fumée (bouton vert)
- ouvrir la vanne d'air comprimée ou brancher la pompe à air
- savoir où se trouve le bouton d'urgence (ou stop/arrêt)


## Réflexion et test

Le but de l'exercice était de réaliser une **lampe en Polypropylène translucide** en utilisant uniquement une planche de **55x65cm et  d'1mm d'épaisseur**, en faisant le moins de chute possible et en utilisant aucuns systèmes d'accroches tel que de la colle, des vis, etc...

Devant travailler une **lampe destinée aux enfants**, je voulais travailler essentiellement sur la projection lumineuse et comment arriver à avoir des rayons et faisceaux lumineux intéressants.

J'ai commencé ma recherche en croquis puis je suis passé au **test avec des feuilles de papier brouillon**. J'ai taillé des lamelles de papier afin de créer des fentes et permettre l'apparition de faisceaux (_photo de gauche_). J'ai ensuite décidé de tester en les tirant vers l'intérieur et vers l'extérieur de la lampe de manière aléatoire mais cela ne changer pas énormément le résultat (_photo de droite_).

![](../images/collagelampe1.jpg)

## Première impression Laser (LASESAUR)

1. **Autocad**

Une fois mon prototype papier réalisé, je décide de **tracer mon fichier laser sur Autocad** en DWG pour ensuite l'imprimer au laser. Pour cela je trace le patron de ma lampe en veillant bien à **créer un calque Gravure** d'une couleur et un **calque Découpe** d'une autre couleur.

![](../images/dwg_lampe1.jpg)

[fichier DWG](../images/lampe_LASER1.DWG)

2. **Conversion SVG**

Une fois le fichier terminé, je dois le **convertir en SVG**. Pour cela je me rend sur un [site](https://anyconv.com/fr/convertisseur-de-dwg-en-svg/) de convertisseur DWG en SVG. Je n'oublie pas aussi de prendre rendez-vous sur [Fabman](https://fabman.io/login?unknownInvitation=true) pour **réserver un créneau d'impression** sur une des 2 machines du Fablab.

3. **Inkscape**

Je dois ensuite aller sur **Inkscape** sur l'ordinateur lié à la machine Laser, je dois **changer les px en cm (ou mm)** pour mettre mon dessin à l'échelle, modifier le format si celui présent n'est pas assez grand et bien vérifier que ma dimension W en haut est juste (_bien verrouiller le cadenas de proportions_)

4. **Impression**


>1.  Tout d'abord, j'ouvre la machine pour y placer ma feuille. Avec le **canevas** du Fablab des paramètres de découpe et gravure, je choisis ceux qui convienne à mon impression.

![](../images/collagelaser1.jpg)

>2. Ensuite, je vais sur l'ordinateur, j'ouvre le **DriveboardApp** de l'imprimante (_logo Dinosaure_) et je clique sur **Open**. Je vérifie que mon fichier rentre sur la feuille en cliquant sur le l'**icône deux flèches** et je regarde dans la machine si le laser fait bien le tour de ma feuille sans dépasser. Une fois cette vérification faite, je peux placer des supports métalliques le long de ma feuille afin qu'elle ne bouge pas durant la découpe.
J'entre dans les paramètres de gauches les données de gravure et de découpe. Je clique ensuite sur le **+** pour choisir la couleur de mon calque utilisé.
Une fois tout cela fait, je peux cliquer sur **RUN** afin de lancer l'impression. **Je veille à bien refermer la machine et à allumer les extracteurs de fumée ainsi que la vanne.**

![](../images/collagelaser2.jpg)

**A noter** : j'ai eu plusieurs problèmes car mon impression était longue et j'avais énormément de découpe sur mon fichier à intervalle serrés et donc la machine s'est arrêtée une première fois car elle chauffait trop et la deuxième fois j'ai du utiliser le bouton d'arrêt d'urgence car il y avait trop de fumée et cela devenait dangereux. J'ai cependant pu récupérer ma lampe qui était quasiment finie et j'ai juste dû découper une petite partie au cutter afin de pouvoir la présenter au jury.


**Voici le résultat !!**

![](../images/impression1111.jpeg)

![](../images/jury.jpg)

## Correction

Suite aux remarques du **jury du 29/10/2021**, il m'a été dit que les projections et faisceaux étaient intéressants mais que la hauteur de la lampe n'avait pas grand intérêt ainsi que le socle arrondi qui n'était pas adapté et n'homogénéisé pas le projet. Les fentes en diagonales n'apportaient pas non plus grand intérêt aux projections lumineuses.

J'ai donc décidé de retravailler mon projet de lampe suites aux remarques et donc d'en réimprimer une nouvelle, plus travaillée. Je l'ai rétréci en hauteur, et j'ai conservé seulement une diagonale pour mettre le mouvement des lamelles sur les face et j'ai créé un nouveau socle intégré à la lampe avec un système de cache intégré.

## Deuxième impression Laser (EPILOG)

1. **Autocad**

J'ai de nouveau travaillé sur Autocad mon fichier de ma nouvelle lampe en utilisant mes calques **Gravure** et **Découpe**.

![](../images/dwg_lampe2.jpg)

[fichier DWG](../images/lampe_LASER2.DWG)


2. **Conversion SVG**

Je **convertis** de nouveau mon fichier DWG en SVG en allant sur le [site](https://anyconv.com/fr/convertisseur-de-dwg-en-svg/) de convertisseur DWG/SVG.

3. **Inkscape**

Je remet mon fichier sur Inkscape comme décrit au-dessus pour pouvoir le transférer ensuite sur la machine.

4. **Impression**

>1. Tout d'abord, j'ouvre le **Epilog Dashboard** sur l'ordinateur lié à la machine, j'ouvre **Fichiers** et je sélectionne mon document SVG. Ensuite, dans la colonne de droite, je clique sur ** Autofocus** pour choisir **Palpeur**. Mes deux calques apparaissent. Je viens sélectionner le **calque vert**, le calque Gravure ici, et je choisis dans Type d'opération **Découpe** (même si c'est de la gravure).

![](../images/collageepi1.jpg)

>2.  Je regarde le canevas lié à l'imprimante laser pour **déterminer la vitesse et la puissance** du laser pour ma découpe. Je change les données pour le **calque Rouge** en mettant **vitesse 10 et puissance 20** et je laisse la **fréquence sur 100%**. Ce seront mes parties gravées.
Pour le **calque Vert** je met **vitesse 80 et puissance 10** et je laisse aussi la fréquence sur 100. Pour finir, il faut cliquer en bas à droite sur **Print**.

![](../images/collageepi2.jpg)

>3. Le travail apparaît alors en haut de la liste sur l'écran de la machine. Avant de lancer l'impression, il faut **allumer l'extracteur de fumée** (_la grosse machine à droite de l'ordinateur_) en appuyant sur le **bouton de gauche**.

![](../images/collageepi4.jpg)

>4. Pour **démarrer la découpe**, sélectionnez le fichier puis appuyez sur **Play** (_bouton noir et blanc_). Ca y est l'impression est lancée !!

![](../images/collageepi3.jpg)


## Résultat final


![](../images/collagefinale1.jpg)

![](../images/collagefinale2.jpg)
