# MODULE 2_Conception Assistée par Ordinateur

_Dans ce nouveau module, l'exercice est de se familiariser avec le logiciel **Fusion360**, un produit Autodesk téléchargeable [ici](https://www.autodesk.com/products/fusion-360/free-trial). Après avoir eu une introduction présenté par Thibaut Baes, il nous était demandé de reproduire un objet du Brussel Design Museum ou personnel en fonction de notre projet final. Faisant partie de la team travaillant au musée, j'ai choisi  le **H-HORSE, dessiné et designé par NENDO**._

## L'objet

![](../images/fusion1.png)

_source : photos Camille Brianchon_


> "Les « poutres en H » en acier utilisées dans les grandes structures telles que les gratte-ciel et les ponts sont très solides et très efficaces. En section transversale, ils ont littéralement la forme d'un « H » et c'est cette caractéristique qui donne à la structure sa résistance mécanique. En appliquant ce concept directement au cheval à bascule d'un enfant, une forme a été créée qui affiche à la fois fonction et force, même avec une utilisation minimale de matériaux. Transformer l'utilisation de ce composant pratique en une utilisation différente avec un sens ludique est le processus de conception qui remonte à l'histoire de la société Kartell. Cette entreprise a commencé par fabriquer et vendre des équipements de laboratoire hautement fonctionnels et est maintenant engagée dans la fabrication de beaux meubles et accessoires en plastique admirés dans le monde entier."


![](../images/fusion2.jpeg)

_source : https://www.nendo.jp/en/works/h-horse-2/_


## L'interface Fusion360

![](../images/fusion3.png)


Voici, pour commencer, un récapitulatif des **commandes de base** présentes sur la zone de travail de Fusion.

1. _les outils de modélisation_

Ils vont permettre le **dessin en 2D** avec l'esquisse par exemple, mais aussi toutes les possibilités de **dessin 3D** comme l'extrusion, la révolution, le balayage, etc... Cette ligne de commandes permet aussi d'inspecter les propriétés de dessin, de mesurer mais aussi d'insérer des images ou encore diverses possibilités de sélection. A vous d'en découvrir plus directement sur Fusion360 ;)

2. _les axes_

Ce **cube** en trois dimensions permet de sélectionner rapidement le **plan** dans lequel nous voulons travailler en 2D. Il suffit tout simplement de cliquer sur la face qui nous intéresse (exemple : devant ou dessus). **L'icône maison** quand à elle permet de revenir sur une vue globale de l'objet en 3 dimensions.

_Remarque : si on veut simplement tourner autour de l'objet, il suffit de choisir l'outil **Orbite** en bas de l'écran et l'outil **Main** si on veut uniquement bouger verticalement ou horizontalement._

3. _la ligne du temps_

A chaque nouvelle étape de modélisation et de conception, une **icône** s'affiche en fonction de la commande qui a été utilisée (icône Esquisse ou Extrusion par exemple). Grâce à cela, il est possible de **déplacer le curseur** en fin de ligne et de le remonter à l'étape que l'on souhaite modifier ou supprimer.


## Modelisation 3D

#### Etape 1

Je commence par sélectionner l'outil **Esquisse** et je choisis le plan dans lequel je dois dessiner mon objet. Je trace le bas de l'objet avec l'outil **Arc de cercle** et je le décale de la distance dont je souhiate en utilisant l'outil **Décaler**. Pour la partie supérieure courbe de l'objet, je choisis l'outil **Spline** et je trace ma ligne en une seule fois. Je viens également la décaler. Cela me créer deux face distinctes que je vais pouvoir étirer par la suite.
Pour finir mon dessin, je n'oublie pas de cliquer sur **Terminer l'esquisse** en haut à droite pour pouvoir continuer le dessin en 3D.

![](../images/obj1.jpg)

#### Etape 2

Je viens sélectionner l'outil **Extrusion** pour venir étirer ma face et créer l'objet. Pour cela je clique sur l'icône, je sélectionne ma face, je tire dans la direction horizontale que je souhaite et j'**encode la distance** souhaitée. Je répète cette action de l'autre côté de ma face.  

![](../images/obj2.jpg)

![](../images/obj3.jpg)

#### Etape 3

Toujours en utilisant l'outil **Extrusion**, je viens étirer ma face inférieure du bas de ma objet pour créer la partie à bascule du H-HORSE. Je viens, comme expliquer avant et montrer dans la photo suivante, **entrer la valeur** de décalage souhaitée.  

![](../images/obj4.jpg)

![](../images/obj5.jpg)

#### Etape 4

Pour cette étape, il faut assembler les 2 parties créées pour finaliser l'objet dans sa totalité. Je clique sur **Esquisse** de nouveau pour **retourner en 2D** et **créer les contours et la face** de ma partie centrale.
Je viens simplement relier les extrémités de mes deux parties courbes avec l'outil **Ligne**. Si les points sont bine reliés, la face devient **bleue** et je vais donc pouvoir, par la suite, l'étirer pour l'épaissir. Je n'oublie pas de cliquer sur **Terminer l'esquisse**.

![](../images/obj6.jpg)

#### Etape 5

Une fois de retour dans mon **espace 3D**, je vois ma nouvelle face apparaitre en bleue.  

![](../images/obj7.jpg)

Comme les étapes précédentes, j'utilise de nouveau l'outil **Extrusion** pour venir l'épaissir de chaque côtés avec la distance voulue.

![](../images/obj8.jpg)

Je viens ensuite cliquer sur le **dossier Esquisse** à gauche et appuyer sur **l'oeil** pour désactiver les traits de dessin.

![](../images/obj10.jpg)

L'objet est terminé !! :)

Cependant j'ai voulu pousser le travail un peu plus en profondeur par curiosité et dans le but de **découvrir des nouvelles fonctionnalités de Fusion**.

#### Etape 6

En haut à gauche, je clique sur la flèche à droite de Conception et je viens choisir **Rendu**.

![](../images/obj11.jpg)

J'arrive sur l'espace de travail de rendu et je commence par cliquer sur **l'icône Apparence** (_le rond avec les couleurs_) dans l'espace Configuration. Une fenêtre apparait et je vais pouvoir explorer et **choisir la matérialité** de mon objet dans la bibliothèque existante de Fusion.

![](../images/obj12.jpg)

Une fois choisie, je clique et maintien appuyer ma souris sur le matériau et je le fais tout simplement **glisser sur mon objet**.  
Ensuite, je viens cliquer sur **l'icône Paramètres de scène** pour modifier la luminosité, l'environnement, les ombres, etc...

![](../images/obj13.jpg)

Quand la matérialité est choisie ainsi que les autres paramètres, je clique sur sur la première icône dans **Rendu dans le canevas** et une nouvelle fenêtre apparait. Je viens cocher **verrouiller la vue** afin que mon rendu ne bouge pas si je zoome ou dézoome par exemple, je choisis également la **résolution** et je clique sur **ok** pour lancer mon rendu final.

![](../images/obj14.jpg)

Voici le rendu une fois le chargement terminé !!

![](../images/obj15.jpg)

Je peux créer une image JPEG ou PNG juste de mon objet seul en cliquant sur la 3ème icône **Capturer l'image**, choisir **fenêtre active** puis **l'emplacement** et mon image s'exporte automatiquement : voici le résultat :)

![](../images/obj16.jpg)

#### Fichier STL

[Mon fichier](https://a360.co/3HnA1we)

#### Tutos

- [Les bases pour Fusion360](https://www.youtube.com/watch?v=ExHGnBdvz7o)
- [Notions et guide](https://formlabs.com/fr/blog/fusion-360-conseils-bases-impression-3d/)
