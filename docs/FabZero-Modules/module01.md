# MODULE 1_Documentation


_Dans ce module 1, nous devions configurer **GitLab** puis savoir s'en servir afin de répertorier et créer notre propre espace de travail où toutes les informations de travail et de mon projet final seront expliquées et accessible sur mon site web. Cet outil permet de partager mon travail mais aussi toute mon évolution._
</div>


## Installation de GitLab

Pour pouvoir utiliser GIT sans devoir passer par la plateforme internet, il faut l'installer sur son ordinateur.
Pour cela, il faut suivre le [lien](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#configure-git) suivant.

Quelques **explications** sont cependant nécessaires.
Tout d'abord, pour MAC, il faut se rendre sur le site Git et installer HomeBrew en entrant dans le terminal de l'ordinateur le code suivant : ```/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"```

Une fois cette étape faite, il est alors possible de **télécharger Git** [ici](https://sourceforge.net/projects/git-osx-installer/files/git-2.18.0-intel-universal-mavericks.dmg/download?use_mirror=pilotfiber).

Une fois le téléchargement et l'installation de Git faite sur l'ordinateur, il est quand même recommander de vérifier sa bonne installation. Pour cela, je tape dans le terminal la commande suivante ```git --version```  afin de voir si mon ordinateur trouve bien la commande. Il apparait alors ```git version 2.33.0``` , tout est donc ok.

Une fois installé, il faut s'identifier pour déterminer qui utilise Git et donc il faut que je rentre mes **données personnelles** (que j'ai préalablement utilisé dans mon espace personnel Git en ligne) dans le terminal à l'aide des commandes suivantes :

`git config --global user.name "camille brianchon"`

`git config --global user.email "camille.brianchon@ulb.be"`

Pour terminer la configuration , je m'assure que mes données ont bien été prises en compte et je rentre dans le terminal la commande : `git config --global --list`

Et voilà, cette étape est finie !!

## Clé SSH

Dans cette seconde partie, les explications suivantes vont permettre de créer une clé SSH qui, par la suite, fera tout simplement le lien entre GitLab et notre ordinateur.

Premièrement, il faut se rendre sur Gitlab, aller dans **Settings**, cliquer sur **SSH** et appuyer sur **Generer une clé**.

![](../images/screen1.png)

Ensuite, je me rend sur cette [page](https://docs.gitlab.com/ee/ssh/index.html#generating-a-new-ssh-key-pair) qui détaille parfaitement toute les étapes. Je suis donc le processus pour une **RSA SSH Keys**.


Pour créer ma clé personnelle, j'ouvre le terminal et j'entre la commande suivante :  </div>
`ssh-keygen -t rsa -b 2048 -C "email@example.com`

Je remplace avec mon adresse mail :

`ssh-keygen -t rsa -b 2048 -C "camille.brianchon@ulb.be"`

Mon terminal me propose alors de générer une clé publique/privée. J'appuie sur la touche **Enter** de mon clavier et il me demande ensuite où je souhaite enregistrer ma clé. Je choisis le fichier, j'appuie sur **Enter** une seconde fois, je choisis un **code** et je refais **Enter** pour le confirmer après l'avoir choisi.
Voici en image ce que le terminal m'affiche.


![](../images/screen2.png)

Une fois ma clé SSH créer sur mon ordinateur, je dois la créer sur Gitlab. Pour cela, j'ouvre le fichier créer pour ma clé dans un éditeur de texte (Atom par exemple) et **je la copie**.
Je vais alors sur **Gitlab --> Settings --> SSH**.

![](../images/screen3.png)

Je colle alors dans la zone de texte, je lui donne un nom (pas obligatoire) et je clique sur **Add Key**.
Ca y est la clé est créée et configurée :)

![](../images/screen4.png)


## Installation et configuration d'Atom

Afin de ne plus passer par la plateforme internet Gitlab et travailler mes modules et ma documentation directement via mon espace ordinateur, je dois télécharger un éditeur de texte, **Atom**. Ensuite quelques manipulations seront utiles à faire afin de l'utiliser.  

Pour **télécharger Atom** rien de plus simple que de se rendre sur leur [site](https://atom.io/) et de cliquer sur **Download**.

Premièrement pour utiliser Atom et éditer mon travail Git directement sur mon ordinateur, je dois **créer un dossier** avec mes informations Gitlab.

Je me rend alors sur mon espace Gitlab et je clique sur **Clone** dans mon espace projet et je copie l'URL **clone avec SSH**.

![](../images/screen5.png)

J'ouvre alors mon terminal et je tape `cd /Users/camillebrianchon/Desktop/FABGIT` pour determiner l'emplacement sur mon bureau (j'avais créer juste avant un dossier FABGIT sur mon bureau). Pour y mettre mes informations je tape `git clone + l'URL que j'ai copié juste avant`puis j'appuie sur **Enter**.
Le terminal me demande confirmation de copie, je tape `yes`pour approuver. Les fichiers sont alors bien copiés dans mon dossier FABGIT.

Voici un récapitulatif de ce que mon terminal affiche pour le clonage :

![](../images/screen6.png)

Il faut maintenant comprendre Atom et savoir faire le lien entre notre travail sur l'ordinateur et celui sur Gitlab.
Je commence par ouvrir **Atom**, puis **Open a project** et je sélectionne le fichier que j'ai envie de modifier dans mon nouveau dossier cloner.

![](../images/screen7.png)

Je peux à partir de maintenant modifier mes fichiers directement sur Atom et les envoyer sur Gitlab et inversement.

Pour faire venir les information de Gitlab comme les nouvelles images par exemple, Il faut appuyer sur **Fetch** (en bas à droite de la fenêtre Git), entrer son mot de passe de clé SSH et puis appuyer sur **Pull**.

![](../images/screen8.png)
![](../images/screen11.png)
![](../images/screen10.png)

Pour faire l'inverse, c'est-à-dire envoyer nos modifications faites sur notre ordinateur via Atom sur Gitlab, il faut premièrement cliquer sur **File** -> **Save**. Nos modifications arrivent alors dans la fenêtre Git d'Atom dans **Unstaged Changes**. Il faut alors cliquer sur **Stage All**. Ensuite, en cliquant sur **See all staged changes**, où je peux verifier ce qui a été modifié ou pas. Une fois la vérification faite, j'entre dans la fenêtre _Commit message_ ce que j'ai modifié (afin de toujours savoir ce qui a été fait) puis je clique sur **Commit to main**. La dernière étape consiste à appuyer sur **Fetch**, d'entrer à nouveau son mot de passe puis de cliquer sur **Push** et le tour est joué, nos modifications ont été envoyées sur Gitlab en un clic ;)

![](../images/screen12.png)


## Configuration du site web

Pour améliorer le visuel du site, il faut changer les données inscrites par défaut sur Gitlab. Pour cela, je me rend sur Gitlab pour les modifier. J'ouvre le fichier **mkdocs.yml** et je clique sur **Edit**. Je peux alors changer le nom du site, le thème, etc...
Pour ma part, j'ai modifié les lignes 2 à 6, ainsi que la ligne 11 en remplaçant **spacelab** par **minty** pour changer le visuel de mon site. J’ai également désactivé les highlights en écrivant **false** pour éviter de mettre des couleurs à tout va dans mes blocs de code.


![](../images/screen15.png)


## Markdown

#### Outils

Afin d'enrichir mon contenu, il est important de connaitre les commandes de rendu pour les éditeurs de texte comme Atom. Voici une liste de celles qui m'ont été utiles :


1. _les titres_
![](../images/screen16.png)
2. _la graphie_
![](../images/screen17.png)
3. _les liens_
![](../images/screen18.png)


#### Insérer une image

En plus des outils de traitement de texte, je peux insérer des images pour rendre le site plus riche et compréhensible. Pour cela, je commence par bien **redimensionner et compresser** mes images sur **Photoshop** ou sur **GIMP** (téléchargeable [ici](https://www.gimp.org/downloads/)).

Ensuite, je me rend sur **Gitlab** dans mon espace projet, je clique sur **docs** puis sur **images** et je peux alors ajouter des images en appuyant sur le **+** puis sur **upload file**.  

![](../images/screen19.png)


Quand je retourne sur Atom pour éditer mon texte, il me suffit de taper la commande `![](../images/(nom du fichier).png ou jpeg` pour **insérer mon image** préalablement télécharger sur Gitlab.
