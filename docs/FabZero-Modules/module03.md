# MODULE 3_Impression 3D

_Dans ce module 3 j'ai appris à utiliser le logiciel **PrusaSlicer**, permettant de faire de l'impression 3D sur les machines **Prusa I3 MK3S**. Ces imprimantes (comme toutes les autres machines du FABLAB ULB sont accessibles sur réservation sur [FabMan](https://fabman.io/login?unknownInvitation=true) en s'identifiant avec ses codes ULB. J'ai donc pu imprimer en 3D mon objet préalablement réaliser sur Fusion360 et décrit dans le module 2._


## Installation PrusaSlicer

Pour pouvoir utiliser le programme, je dois le télécharger et l'installer sur mon ordinateur. Je me rend donc sur le [site](https://www.prusa3d.fr/prusaslicer/) de Prusa et je télécharge la version pour MacOS en choisissant la **version 2.3.3 Drivers & App Package Full Installer**.
Une fois installé, je **clique sur le disque de démarrage** et ca y est Prusa est installé !!


## Importation et réglages d'impression

- **Exportation**

Pour commencer, je dois exporter et enregistrer mon objet Fusion en **fichier STL** sur Fusion 360.

![](../images/obj17.jpg)

[Mon fichier](https://a360.co/3HnA1we)

Une fois l'exportation faite, j'**ouvre Prusa** et je viens tout simplement **glisser mon fichier STL** dans la fenêtre du logiciel.

![](../images/prusa2.jpg)


- **Redimensionnement**

En important mon fichier STL, je me rend compte qu'il est **beaucoup trop grand** et dépasse largement de la plateforme de la machine. Je clique alors sur l'icône **Redimensionner** et je viens le rétrécir en bougeant les points de couleurs en diagonales ou en entrant à droite de l'écran **la valeur d'échelle en %** que je souhaite.  

![](../images/prusa3.jpg)

![](../images/prusa4.jpg)

Une fois redimensionner, je clique sur **découper maintenant** pour connaitre mon temps d'impression (nous avions 2H maximum autorisée). Ici, je vois que je suis déjà à 1h16 alors que je n'ai pas encore fait mes réglages d'impression.

![](../images/prusa5.jpg)

![](../images/prusa6.jpg)

- **Réglages**

En haut de mon écran, je clique sur **réglages d'impression** pour venir changer les paramètres qui m'intéressent.
Il est important de d’abord mettre son logiciel en **mode Expert**. Cette fonction nous permet d’avoir plus de paramètres afin d’avoir plus de précision lors de l’impression de l’objet.

1. _Couches et périmètres_

![](../images/prusa7.jpg)

2. _Remplissage_

![](../images/prusa8.jpg)

3. _Jupe et bordure_

![](../images/prusa13.jpg)

4. _Supports_

Les supports vont permettre de maintenir certaines parties de l'objet durant l'impression pour éviter qu'ils tombent.

![](../images/prusa9.jpg)

5. _Filament_

Je choisis du PLA car c'était le matériau imposé pour le cours et la formation.

![](../images/prusa10.jpg)


- **Vision finale**

Je revient sur le **mode plateau** et je clique sur **découper maintenant** afin de visualiser mon objet avec les supports et de connaitre mon nouveau temps d'impression : _ici il a diminué (50min au lieu d'1h16) grâce à la modification du pourcentage de remplissage (10% au lieu de 15%) et du motif de remplissage_.
Je termine en cliquant sur **Exporter le G-code** pour pouvoir le mettre sur la machine du Fablab.

![](../images/prusa11.jpg)

![](../images/prusa12.jpg)


## Impression 3D

Après avoir exporter mon fichier G-code, je le met sur la **carte SD** de la machine 3D du Fablab en créant un fichier Camille dessus. Ensuite j'insère la carte dans la machine avec le bon fil et colorie choisi, j'ouvre mon dossier et je **sélectionne mon fichier** G-code. L'impression commence alors dès que la machine atteint les **250 degrés**. Je dois bien rester à côté de la machine les 5 premières minutes de l'impression pour être sur que tout se déroule correctement.

![](../images/collage2.jpg)

## Résultat

Une fois l'impression terminée, je dois attendre une quinzaine de minutes avant de **retirer les supports avec une petite pince** et me rendre compte que tout s'est bien déroulé et que mon impression 3D est une **réussite** !!

![](../images/collage3D.jpg)
