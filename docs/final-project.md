# Projet Final : Brussel Design Museum


Tout d'abord, nous **(Eliott, Zoé et Camille)** avons réfléchi à notre angle d'attaque sur ce que nous allions transmettre aux enfants lors de la visite du musée.

Nous avons fait le choix de leur expliquer la manière dont un designer passe d'un objet de base, classique (une bouteille d'eau par exemple) à un objet pratique et plus fonctionnel. Nous devons alors **transmettre aux enfants le cheminement du designer et les choix qu'il a effectué afin d'atteindre son objectif pour concevoir son objet.**

Après avoir axé notre réflexion et le but à atteindre, nous avons penser aux moyens. Il était important pour nous de trouver un moyen ludique, fonctionnel et accessible. Nous avons décidé de s'orienter vers une nouvelle conception de l'audio guide en utilisant la réalité augmentée. Ce moyen est, pour nous, une évolution non seulement audio mais aussi visuelle.


## Première semaine (18.11.2021)

Les questions primordiales à se poser avant de commencer ont été :

- _Qu'est-ce que l'enfant aura reçu grâce à notre projet en sortant du musée?_
- _L'audio guide doit être que pour les enfants ? peut-être pour les parents aussi ?_

On a donc décidé après réflexion que nous allions créer une **application** comportant la **visite en AR** (réalité augmentée) et une section reprenant les objets du musée en 3D avec la possibilité de les projeter en AR chez soi à n'importe quel moment.


#### Premier scénario :
_Guide audio, visuel, manuel et interactif_

> 1. Arrivée de l’enfant au musée avec ses parents
2. Soit il reçoit une tablette numérique, soit il utilise le smartphone des ses parents
3. Ouverture de l’application préalablement téléchargé, télécharger sur le moment ou déjà installé sur les tablettes du musée et mettre les écouteurs
4. Sélectionner le mode Visite en Réalité augmenté sur l'application : PLastico (le robot créé informe et commence la visite)
5. Explication du fonctionnement de la visite : l’enfant reçoit une valise à l’accueil composé de compartiments avec des sachets de pièces détachées. Ensuite il devra suivre les instructions de PLASTICO qui le guidera au premier stop en montrant une image de l’endroit
6. 1er stop : message « es-tu bien devant l’objet ? » si oui scanne le (l’application reconnaitra automatiquement s’il s’agit du bon objet.
7. L’audio dira « es-tu prêt à m’écouter? » - V vert ou X rouge (si V vert, l’audio commence et explique en 1min l’objet afin d’intéresser l’enfant avec des interactions pour stimuler l’enfant et garder sa concentration) si X rouge la voix repose la question après 10 secondes.
8. A la suite de la première minute d’explications du premier objets quelques questions sur ce qu’il a retenu lui sont posées (tes avec V vert X rouge ) genre « cet objet est-il en plastique mou? Peut-on s’asseoir dessus? Etc… » l’enfant doit y répondre, si réponse fausse alors une nouvelle petite explication audio démarre. Incitation à observer l’objet dans la réalité en faisant dire à PLastico que l’enfant doit tourner autour, se rapprocher, etc..
9. Plastico et son ami lui explique la suite c’est à dire monter l’objet qui est dans sa valise en pièces détachées (puzzle impression 3D). Icône d’aide sur l’appli
10. Plastico et son ami lui explique de poser l’objet sur un socle prévu à cette effet avec un point rouge au sol d’où il devra le scanner afin que l’application valide le montage =>  V vert s’affiche si tout est ok ( bon endroit et bon montage), si pas Plastico lui dira de réessayer pour pouvoir passer au prochain objet. après avoir posé l’objet, la nouvelle scène commence immédiatement et on demandera à l’enfant de récupérer son objet à la fin avant d’aller dans l’autre salle.
11. ??? problème regard à travers l’écran  
12. Une fois validé, PLASTICO annonce la deuxième étape en montrant une photo de l’objet suivant à trouver dans le musée et le guidera.
13. ??? Comment guider ?
14. Idem
15. Idem
16. Idem
17. A mi parcours, PLASTICO invite l’enfant à se promener seul dans le musée => l’écran devient noir par exemple et PLastico lui dit qu’il va faire une sieste (environ 4-5min) et qu’il devra être prêt pour la prochaine étape ==> objectif d’inciter l’enfant à découvrir par lui meme les autres objets non décrits dans l’audio guide
18. PLASTICO revient et demande si l’enfant a eu assez de temps : V ou X (minutes suppl. si choix X)
19. Idem que les autres étapes précédentes : PLASTICO montre une photo de l’objet à aller découvrir
20. Idem
21. Idem
22. Idem
23. Idem   
24. A la fin du parcours, PLASTICO le félicite et lui indique d’aller chercher sa recompense à l’accueil ou au shop en y montrant sa valise complétée.

#### Debrief

Après avoir discuté et échanger sur notre première proposition avec Denis et Victor, voici les points clés que nous devions régler et résoudre à ce moment la :

- Vision parent : "j’ai envie d'apprendre quelquechose à mes enfants, et le faire avec l’audio guide
Il faut donc penser au parents impliqué dans le système

- Clarifier le but pour l’enfant, il faut arriver à être plus radical sur le contenu et ce qu’on apporte à l’enfant
(Angle d’attaque précis a avoir)

- J’ai besoin d’apprendre quoi à l’enfant et à la famille ? Et par quels moyens ?

- Quel est l’intention du musée de base ?

**Il était donc primordial de choisir un axe précis de ce que nous allions faire apprendre aux enfants.**

## Deuxième semaine (25.11.2021)

Suite aux remarques de la première semaine, nous avons choisis comme axe **l'evolution d’un objet de base (le plus basique possible) à un autre**.
**Montrer le chemin entre un objet de base vers un objet amélioré par ses fonctions. On veut donc transmettre les fonctionnalités d’un objet aux enfants.**

Concernant la visite, nous avons tout d'abord choisis un objet du musée pour pouvoir faire notre prototype. Nous avons pris la **chaise Panton**, conçu par Verner Panton.

![](./images/pantone2.jpg)

Nous avons ensuite **chercher les fonctionnalités** de cette chaise et ses particularités qui en font une chaise "design".
Par la suite, nous avons décidé de **créer un personnage fictif** qui serait le narrateur de la visite et donc de l'audio-guide. Eliott a créer virtuellement un robot que nous avons nommé **Plastico** pour pouvoir enchainer sur l'écriture du scénario.

Nous avons créer un storyboard montrant comment se passerait la découverte de la chaise dans le musée par l'enfant et le (ou les) parents.

![](./images/storyboard1.jpg)

![](./images/storyboard2.jpg)

![](./images/storyboard3.jpg)

> Le parent lit une histoire concernant le procédé du designer pour obtenir l’objet concerné avec notre protagoniste : Plastico, la mascotte du musée. Le parent raconte et instruit son(ses) enfant(s) en suivant l’histoire tout en posant des questions à son enfant. Ce dernier devra y répondre du mieux qu’il peut.

Notre visite en AR se finirait par un atelier manuel où les enfants pourraient à leur tour se prêter au jeu d'être designer le temps de quelques minutes.

>À la suite de cette histoire, les enfants et les parents auront eu un premier aperçu du processus mené par le designer et de la forme de l’objet qui en découle.

C’est alors qu’ils participeront à un atelier ou à l’aide d’accroches, hanses, cordes, pate a modeler ils devront choisir un objet de base présent sur la table (bouteille en plastique etc) et designer leur propre solution avec les fonctionnalités qu’ils veulent intégrer à l’objet. Grâce à un panneau les parents seront guidés pour aiguiller leur enfant des étapes à suivre pour l’amélioration de leur objet.


#### Debrief

En fin de compte, avec les professeurs nous avons convenus, comme prévu au tout début, qu'il est plus intéressant que **l'enfant fasse la visite en AR seul** et que les parents fassent leur tour aussi de leur côté et qu'ils se retrouvent à la fin pour échanger et **faire l'atelier ensemble**, le but étant que les enfants apprennent aussi de nouvelles choses à leur parents car ils ne vont pas assimiler les mêmes choses durant leur visite individuelle.

## Troisième semaine (02.12.2021)

Après s'être renseigner sur la manière dont les enfants apprennent, communiquent, lisent et comprennent, on a pu se pencher sur la mise en place de l'AR, du scénario ainsi que de l'atelier final de fin de parcours du musée. Nous avons finaliser le scénario autour de la chaise Panton en **ciblant 3 axes de fonctionnalité de l'objet en les comparant à une chaise classique et basique** : sa forme qui lui procure un empilement horizontal facile plutôt que vertical, sa matérialité qui lui permet d'être exploitable autant à l'extérieur qu'à l'intérieur et facilement nettoyable et enfin le fait qu'elle soit en un seul bloc ce qui lui permet d'être stable en son socle même si elle n'a pas 4 pieds.

Eliott a ensuite monté toute la partie informatique, c'est à dire modéliser les éléments, créer les animations et finaliser l'AR. Nous avons enregistrer notre scénario pour monter notre audio pour l'inclure dans l'AR à la première personne en faisant raconter la description de Panton à Plastico afin de rendre la visite plus ludique et enrichissante.

En ce qui concerne **l'atelier final**, nous avons réfléchi à comment faire comprendre aux enfants le processus d'évolution d'un objet de base (classique) à un objet designer et penser de manière plus fonctionnel que les précédents.

Nous avons tester avec une enfant de 5ans et demi lors d'un baby-sitting pour voir et **comprendre comment l'enfant appréhende et réfléchit face à deux objets différents mais ayant le même but final.**

![](./images/bbysit.jpg)

## Journée test avec les enfants

La journée du 2 décembre, nous avons reçu les élèves de la classe de Yann pour leur faire **tester nos projets et nos prototypes**.
Ils étaient par groupe de 2 ou 3, âgés de 6 à 7ans. Nous les avons fait commencer par la découverte de la chaise Panton par Plastico en AR sur nos téléphones afin qu'ils comprennent les démarches de design de cet objet. Ensuite nous avons discuter avec eux de leur ressentis et de leurs question.

Dans un second temps, nous leur avons fait tester l'**atelier participatif**.
Il y avait sur une table 3 objets : une bouteille en plastique (comme étant l'objet de base, classique) et deux gourdes différentes.
Devant eux, nous avions créé au fablab préalablement 2 planches (une avec divers mots et l'autre avec des scratch). L'objectif était que pour chacune des 2 gourdes ils choisissent 4-5 mots identifiant les fonctionnalités qui avaient été ajoutées ou modifiés par le designer afin de les rendre plus fonctionnelles.

![](./images/jeu1.jpg)

A la fin, nous leur avons proposé de **prendre une photo souvenir avec Plastico ou la chaise Panton**. Le but est de leur montrer que les objets qu'ils voient en AR et particulièrement la chaise (et dans le futur d'autres objets du musée) soit projetable partout et qu'il puisse **se rendre compte de l'espace que l'objet prend, qu'ils puissent s'en approcher, tourner autour, etc...**

![](./images/jeu2.jpg)

#### Debrief

A la fin de la journée, nous avons fait le point sur ce qui fonctionnait ou pas :

- concernant **l'AR**, les enfants étaient attentifs et enthousiastes grâce à l'audio et aux animations illustrant les dires de Plastico. Certains ont même demander à le refaire plusieurs fois. A la fin de chaque test AR, nous faisions un debrief de ce qu'ils avaient retenus et grâce aux petites animations, ils avaient retenus les nouvelles fonctionnalités et les changements apportés.Nous décidons donc de **continuer de développer l'AR pour d'autre objets du musée car c'est la majeure partie de notre projet et notre ligne conductrice**.

- pour l'**atelier (le jeu)**, nous nous sommes vite rendus compte que sans accompagnement et sans nous, les enfants ne pouvaient pas faire le jeu seul car ils ne savent pas lire et ce n'était pas assez clair pour eux. Nous avons donc du les aiguiller pendant l'exercice, ce qui ne devait pas être le cas c'est pourquoi **nous ne continuerons pas de développer cet aspect du projet**.  

- Ayant vu que la **photo souvenir** avec l'objet et/ou Plastico, nous décidons de développer ce côté-la pour la finalité de notre projet. Ils pourront le faire seul ou avec leur classe ou leur parents, famille, etc. Le but est donc de créer une **"sharing box"** où l'enfant pourrait se prendre en photo avec l'objet de son choix.


## Pré Jury (16.12.2021)

En vue du debrief de la semaine où nous avons échanger et tester notre projet avec les enfants, nous avons choisis de développer l'**AR**
[(voir GIT Eliott)](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-design/students/eliott.steylemans/) ainsi que le projet de la **Photobox**, en se servant des outils du Fablab à notre disposition.

_En ce qui concerne la partie AR, tout est détaillé sur le Git d'Eliott (voir plus haut)._

Nous nous sommes penchées à deux sur la reflexion au sujet de l'**approfondissement des histoires et de notre axe de recherche** mais aussi sur comment nous allions **construire cette Photobox.**

Nous avons continuer à chercher des **nouveaux scénarios** possible pour l'AR en écrivant les histoires selon notre sélection d'objets faite au musée.

![](./images/histoire1.jpg)
![](./images/histoire2.jpg)

Ensuite, il a fallu réfléchir à la création de la **Photobox**, en pensant à la hauteur, aux dimensions, à comment prendre la photo avec Plastico.
Nous avons donc tracer le fichier laser sur **Autocad** afin de le découper au laser et de l'assembler par la suite.

![](./images/dwg1.jpg)

#### Jour J

Le jour du pré-jury, nous avions amené une chaise Panton pour pouvoir scanner l'objet en live et faire faire l'AR aux jury, la Photobox était installé de manière à ce qu'ils puissent prendre une photo avec PLastico et nous avions imprimé des mockup de ce à quoi les photos finales ressembleraient après impression.

![](./images/photobox1.jpg)

![](./images/photobox2.jpg)

![](./images/mock1.jpg)

![](./images/mock2.jpg)

Nous avions également créer un **prototype papier du fonctionnement de la Photobox sur Ipad.**

![](./images/proto1.jpg)
![](./images/proto2.jpg)


#### Debrief

Les remarques du jury ont été globalement très **positives et encourageantes**. Nous avons relevé des **points importants et utiles** pour l'évolution de notre projet afin qu'il soit le plus abouti possible pour le jury final :

>- peut être tout répertorier et classer par catégorie pour ensuite que l’enfant choisisse ce qu’il veut apprendre
- Un peu trop long l’histoire panton
- Insister sur la diffusion
- Trouver un support pour mettre le telephone genre volant wii
- Communiquer l’experience a fond
- Faire story board pour chaque objet en développant les animations
- Faire parler les objets plutôt que plastico ?
- Designer un objet plutôt qu’un robot ??
- Note explicative derrière la photo en fonction de la visite qu’il a fait ou de l’objet qu’il a choisit
- Question du parcours a plus détaillé
- Analyser les questions pratiques (voir si c’est iPad du musée ou pas?)
- Bien finir tout le scénario
- Illustrer le porte a faux ou superposé un S dessus  dans l’AR
- Possibilité que l’enfant puisse s’approcher ??
- Peut être seulement 30sec-1min pour montrer une caractéristique importante

## Semaines finalisation du projet (du 10.01.2022 au 26.01.2022)

_Nous avons repris point par point les remarques du préjury afin de répondre au mieux à ce qui ne fonctionnait pas ou à ce qui manquait._

#### La photobox

Il nous manquait un **support pour la poser**, ce qui était notre contrainte principale si nous voulions nous installer au musée à l'endroit où Terry nous l'avait conseillé.
Nous avons donc réfléchi à **un pied** facile à monter, transporter et qui ne soit pas pour autant imposant dans le hall du musée.

![](./images/pied1.jpg)

Nous avons aussi voulu ajouter des **lampes LED télécommandé** afin de donner un meilleur éclairage.

![](./images/lampeLED1.jpg)


#### La manette

Nous avions eu comme idée de **créer un support pour le téléphone** afin que les enfants l'ai tout le long de la visite. Nous avons donc **créer une manette** (un peu comme celle des jeux vidéos) afin de sécuriser le téléphone mais aussi de **rendre la visite plus ludique.**
Nous l'avons designé puis **produite au fablab** en découpe laser. Nous avons rajouté des **poignets et un fond de coque en caoutchouc mousse.**
Enfin, pour créer une ouverture facile et sécurisée pour mettre et reprendre le téléphone, nous avons opter pour des **aimants** insérés de part et d'autre de la coque.

![](./images/materiau1.jpg)
![](./images/manette1.jpg)
![](./images/manette2.jpg)
![](./images/manette3.jpg)

**RESULTAT FINAL**
![](./images/manette4.jpg)

#### Le scenario

Enfin, il a fallu complètement réfléchir et détaillé le **scénario de la visite du début à la fin**. Nous sommes donc retournés tous les 3 au musée afin de repérer les endroits où nous allions afficher des panneaux, mettre la Photobox ainsi que refaire une liste des objets que nous allions mettre et détaillé dans nos deux catégories. Nous avons croisé Terry qui nous a refait une petite visite pour nous aider sur **notre axe concernant le plastique**.

Après avoir bien tout analysé et bien fixer nos idées, nous sommes passé à la réalisation des documents.

Nous avons **créer une vidéo introductive** pour l'application [(voir GIT Eliott)](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-design/students/eliott.steylemans/). Il fallait qu'elle soit courte mais qu'elle **explique facilement en animation avec Plastico le fonctionnement de l'application, de l'AR ainsi que de la Photobox et du catalogue**. Nous avons utilisé le logiciel **Adobe Première Pro** après avoir regarder des tutoriels.

![](./images/video1.jpg)

![](./images/video2.jpg)


Ensuite, il fallait indiquer, **dès l'entrée du musée**, la possibilité de faire la visite en AR en téléchargeant notre application.
Nous avions repérer au musée à l'entrée un support métallique dont nous avons relevé les mesures afin de l'emprunter pour le jury et **créer un panneau**.

![](./images/panneau1.jpg)

Pour finir et expliquer le futur de notre projet, nous avons fait un **book détaillant les futures scénario possibles en réalité augmentée**. Nous les avons fait sous forme de **storyboard** qui montre les animations possibles en fonction de ce que l'on souhaite expliquer de l'objet (aussi en fonction de la catégorie dans lequel il se trouve).

_Voici un exemple :_

![](./images/scenario1.jpg)

## Jury final (27.01.2022)

à venir ......
