# A propos de moi

_Bienvenue !!! Vous pourrez suivre ici mon travail et mon évolution à travers les différent modules et voir mon projet final qui sera un travail en lien avec le Brussel Design Museum._



![](./images/photo1_copie.png)

Je m'appelle **Camille Brianchon**, j'ai 24ans et j'étudie actuellement à l'ULB en Architecture à Bruxelles en **Master 2**. N'ayant jamais eu de connaissances et d'enseignement en design, j'ai donc décidé d'intégrer l'option **Architecture et Design**. Cela me permettra d'acquérir de nouvelles compétences.

En dehors des études, je suis sportive, je fais de la boxe thaï depuis peu, je pratique le piano et la guitare, et j'aime m'impliquer depuis longtemps dans des associations sociales et caritatives. Je suis très **curieuse** et toujours à la **recherche de nouvelles expériences et connaissances**.

### Mon parcours

Je viens de **Nice**, dans le sud de la France et ça fait 5ans que je suis à Bruxelles. J'ai intégré l'UCL Loci en première année d'architecture en 2016 mais j'ai décidé de poursuivre mon cursus en 2018 à **l'ULB** pour y finir mon bachelier et master. Je suis particulièrement satisfaite de ce choix car l'ULB m'a apporté un côté créatif et une ouverture d'esprit que je n'avais pas avant.

## Mes compétences

Voici mon **CV** en détails pour en savoir plus sur mes compétences et expériences.


![This is the image caption](./images/CV.png)
